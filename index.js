define(function(require){
	'use strict';

	var prime = require('lib/prime'),
		emitter = require('lib/emitter'),
		options = require('lib/options'),
		$ = require('jquery'),
		neg = function(num){
			return num * -1;
		};

	var lemming = prime({
		// single element prime with events
		implement: [emitter],

		constructor: function(options){
			this.element = options.element;
			this.direction = options.direction || 'r';
			this.pos = options.pos || 0;
			this.step = options.step || 1;
			this.letter = 'a';
		}
	});


	var lemmings = prime({

		implement: [options, emitter],

		options: {
			element: null,
			spriteWidth: 32,
			spriteHeight: 32,
			spriteContainerWidth: 256,
			spriteExplodeContainerWidth: 512,
			sprites: {
				explode: 'http://fragged.org/wp-content/themes/envy/images/lemming_explode_s.gif',
				l: 'http://fragged.org/wp-content/themes/envy/images/lemming_walk_l.gif',
				r: 'http://fragged.org/wp-content/themes/envy/images/lemming_walk_r.gif'
			},
			drawSpeed: 50,
			marginTop: 42,
			spriteZindex: 1000,
			boredTimeout: 3000
		},

		constructor: function(options){
			this.setOptions(options);
			this.fireworks = [];
			this.lemmings = [];
			this.element = $(this.options.element);

			if (!this.element.length){
				return;
			}

			this.elementSize = {
				x: this.element.width(),
				y: this.element.height()
			};

			console.log(this.elementSize);

			return this.trigger('ready');
		},

		create: function(direction, pos, step){

			var self = this,
				index,
				lem = new lemming({
					element: $('<div></div>').css({
						backgroundRepeat: 'no-repeat',
						backgroundPositon: '0 0',
						width: this.options.spriteWidth,
						height: this.options.spriteHeight,
						position: 'absolute',
						float: 'left',
						marginTop: this.options.marginTop,
						zIndex: this.options.spriteZindex
					}).on('click', function(){
						self.explode(self.lemmings.indexOf(lem));
					}),
					direction: direction || 'r',
					pos: pos || 0,
					step: step || 1,
					letter: 'a'
				});

			this.lemmings.push(lem);

			index = this.lemmings.indexOf(lem);

			this.add(index);

			return index;
		},

		add: function(index){
			var lem = this.lemmings[index];


			lem.element.css('margin-left', lem.pos + 'px');
			this.element.append(lem.element);
			this.loadSprite(index, lem.direction);

			return this;
		},

		loadSprite: function(index, spriteIndex, frame){
			this.lemmings[index].element.css({
				'background-image': ['url(', this.options.sprites[spriteIndex], ')'].join('')
			});
			this.setFrame(index, frame || 0);

			return this;
		},

		setFrame: function(index, frame){
			this.lemmings[index].element.css({
				backgroundPosition: neg(frame * this.options.spriteWidth)
			});

			return this;
		},

		move: function(index, delay){
			var lem = this.lemmings[index],
				o = this.options,
				self = this;

			lem.timer = setInterval(function(){
				// framemax
				var nl,
					spriteOffsetX = parseInt((lem.element.css('backgroundPosition') || '0 0').split(' ')[0], 10),
					newOffset = (spriteOffsetX - o.spriteWidth <= neg(o.spriteContainerWidth)) ? 0 : spriteOffsetX - o.spriteWidth;

				switch (lem.direction){
					case 'r':
						nl = lem.pos + lem.step;
						break;
					case 'l':
						nl = lem.pos - lem.step;
						break;
					case 'explode':
						nl = lem.pos;
						break;
				}

				lem.pos = nl;

				if (lem.direction == 'r' && lem.pos >= self.elementSize.x - o.spriteWidth){
					lem.pos = self.elementSize.x - o.spriteWidth;
					lem.direction = 'l';
					self.loadSprite(index, lem.direction, 0);
				}

				if (lem.direction == 'l' && lem.pos <= 0){
					lem.direction = 'r';
					self.loadSprite(index, lem.direction, 0);
				}


				lem.element.css({
					// transform: 'translateZ(' + nl + 'px)',
					marginLeft: nl,
					backgroundPosition: newOffset + 'px 0px'
				});

				lem.trigger('move', index);
			}, delay || 35);

			return this;
		},

		goTo: function(index, pos){
			var lem = this.lemmings[index],
				self = this;

			clearTimeout(lem.timer);

			lem.on('move', function(index){
				// console.log(self.lemmings, index);
				if (self.lemmings[index].pos == pos){
					clearTimeout(lem.timer);
					self.trigger('arrive', index);
				}
			});

			this.move(index);

			return this;
		},

		say: function(index, what){
			var lem = this.lemmings[index];
			what || (what = ['?', '!', '#', '@', 'no'][Math.floor(Math.random() * 4)]);

			lem.element.empty().append($('<div><div>' + what + '</div></div>').attr({
				'class': 'lemChat',
				html: '<div>' + what + '</div>',
				opacity: 0
			}).fadeIn());

			return this;
		},

		shutUp: function(index){
			this.lemmings[index].empty();

			return this;
		},

		bored: function(index){
			var lem = this.lemmings[index],
				self = this;

			lem.timer = setTimeout(function(){
				lem.timer = setInterval(function(){
					self.setFrame(index, [2, 3, 4, 5][Math.floor(Math.random() * 3)]);
				}, 1000);
			}, this.options.boredTimeout);

			return this;
		},

		explode: function(index){
			var lem = this.lemmings[index],
				explode = 'explode',
				self = this;

			lem.exploding = true;
			lem.direction = explode;
			this.loadSprite(index, explode, 0);

			lem.element.off();

			clearTimeout(lem.timer);

			this.move(index, 70);

			setTimeout(function(){
				var coords = lem.element.offset();
				lem.element.fadeOut();

				self.coords = self.element.offset();
				self.firework(coords.left + self.options.spriteWidth / 2, coords.top + self.options.spriteHeight / 2);
				lem.exploding = false;
			}, 1000);

		},

		firework: function(x, y){
			var colors = ['#ffffff', '#ffff00', '#00ff00', '#ff0000', '#0000ff', '#ff00ff', '#00ffff'],
				fw = [],
				j = 20,
				f, a, s;

			while (j--){
				a = Math.random() * 6.294;
				s = (Math.random() > 0.6) ? 4 : Math.random() * 4;

				f = $('<div class="spark"></div>').css({
					background: colors[Math.floor(Math.random() * 7)],
					top: y,
					left: x
				}).data({
					y: y,
					x: x,
					dx: s * Math.sin(a),
					dy: s * Math.cos(a) - 4,
					op: 1
				});

				this.element.append(f);
				fw[j] = f;
			}

			this.fireworks.push(fw);
			this.running = false;

			this.runFirework(fw);
		},

		runFirework: function(fw){
			var self = this,
				len = fw.length,
				i = 0,
				f,
				coords = this.coords,
				data, y, x, dx, dy, op;

			for (; i < len; ++i){
				f = fw[i];

				data = f.data();
				y = parseFloat(data.y);
				x = parseFloat(data.x);
				dx = parseFloat(data.dx);
				dy = parseFloat(data.dy);
				op = (data.op || 1) - 0.05;

				y += dy += 0.18;
				x += dx;

				f.data({
					y: y,
					x: x,
					op: op
				});

				if (y < coords.top || y > coords.bottom || x < coords.left || x > coords.right || op <= 0){
					f.remove();
					fw.splice(i, 1);
				}
				else {
					f.css({
						top: y,
						left: x,
						opacity: op
					});

				}
			}

			fw.length && setInterval(function(){
				self.runFirework(fw);
			}, this.options.drawSpeed);
		}

	});


	return lemmings;

});