/*jshint mootools:true */
'use strict';
(function(){
	Number.implement({
		neg: function(){
			return this * -1;
		}
	});
}());

var lemmings = new Class({

	Implements: [Options, Events],

	lemmings: [],

	fireworks: [],

	options: {
		element: null,
		spriteWidth: 32,
		spriteHeight: 32,
		spriteContainerWidth: 256,
		spriteExplodeContainerWidth: 512,
		sprites: {
			explode: "http://fragged.org/wp-content/themes/envy/images/lemming_explode_s.gif",
			l: "http://fragged.org/wp-content/themes/envy/images/lemming_walk_l.gif",
			r: "http://fragged.org/wp-content/themes/envy/images/lemming_walk_r.gif"
		},
		drawSpeed: 50,
		marginTop: 42,
		spriteZindex: 1000,
		boredTimeout: 3000
	},

	initialize: function(options){

		this.setOptions(options);
		this.element = document.id(this.options.element);

		if (!this.element){
			return;
		}
		this.elementSize = this.element.getSize();
	},

	create: function(direction, pos, step){

		var lem = {
			element: new Element("div", {
				styles: {
					backgroundRepeat: "no-repeat",
					width: this.options.spriteWidth,
					height: this.options.spriteHeight,
					position: "absolute",
					float: "left",
					marginTop: this.options.marginTop,
					zIndex: this.options.spriteZindex
				},
				events: {
					click: function(){
						this.explode(this.lemmings.indexOf(lem));
					}.bind(this)
				}
			}),
			direction: direction || "r",
			pos: pos || 0,
			step: step || 1,
			letter: "a"
		};

		this.lemmings.push(lem);
		var index = this.lemmings.indexOf(lem);
		this.add(index);
		return index;
	},

	add: function(index){
		var lem = this.lemmings[index];
		lem.element.inject(this.element);
		lem.element.setStyles({
			backgroundRepeat: "no-repeat",
			backgroundPositon: "0 0",
			marginLeft: lem.pos
		});
		this.loadSprite(index, lem.direction);
	},

	loadSprite: function(index, spriteIndex, frame){
		var lem = this.lemmings[index];
		lem.element.setStyles({
			"background-image": ["url(", this.options.sprites[spriteIndex], ")"].join("")
		});
		this.setFrame(index, frame || 0);
	},

	setFrame: function(index, frame){
		this.lemmings[index].element.setStyles({
			backgroundPosition: (frame * this.options.spriteWidth).neg()
		});
	},

	move: function(index, delay){
		var lem = this.lemmings[index];
		lem.timer = (function(){
			// framemax
			var framemax = lem.exploding
				? this.options.spriteExplodeContainerWidth
				: this.options.spriteContainerWidth;

			var spriteOffset = lem.element.getStyle("backgroundPosition") || 0, spriteOffsetX = spriteOffset.split(" ")[0].toInt();
			var newOffset = (spriteOffsetX - this.options.spriteWidth <= framemax.neg()) ? 0 : spriteOffsetX - this.options.spriteWidth;


			var nl;
			switch (lem.direction){
				case 'r':
					nl = lem.pos + lem.step;
					break;
				case 'l':
					nl = lem.pos - lem.step;
					break;
				case 'explode':
					nl = lem.pos;
					break;
			}

			lem.pos = nl;

			if (lem.direction == 'r' && lem.pos >= this.elementSize.x - this.options.spriteWidth){
				lem.pos = this.elementSize.x - this.options.spriteWidth;
				lem.direction = 'l';
				this.loadSprite(index, lem.direction, 0);
			}

			if (lem.direction == 'l' && lem.pos <= 0){
				lem.direction = 'r';
				this.loadSprite(index, lem.direction, 0);
			}


			lem.element.setStyles({
				marginLeft: nl,
				backgroundPosition: newOffset + "px 0px"
			});
			lem.element.fireEvent("move", index);
		}).periodical(delay || 35, this);
	},

	goTo: function(index, pos){
		var lem = this.lemmings[index];
		clearTimeout(lem.timer);
		lem.element.addEvent("move", function(index){
			if (lem.pos == pos){
				clearTimeout(lem.timer);
				this.fireEvent("arrive", index);
				lem.element.removeEvents("move");
			}
		}.bind(this));
		this.move(index);
	},

	say: function(index, what){
		what = what || ["?", "!", "#", "%", ":(", "no"].getRandom();
		var lem = this.lemmings[index];
		lem.element.empty();
		new Element("div", {
			"class": "lemChat",
			html: "<div>" + what + "</div>",
			opacity: 0
		}).inject(lem.element).fade(1);
	},

	shutUp: function(index){
		var lem = this.lemmings[index];
		lem.element.empty();
	},

	bored: function(index){
		var lem = this.lemmings[index];
		lem.timer = (function(){
			lem.timer = (function(){
				this.setFrame(index, [2, 3, 4, 5].getRandom());
			}).periodical(1000, this);
		}).delay(this.options.boredTimeout, this);
	},

	explode: function(index){
		var lem = this.lemmings[index];

		lem.exploding = true;
		lem.direction = 'explode';
		this.loadSprite(index, "explode", 0);
		lem.element.removeEvents();
		clearTimeout(lem.timer);
		this.move(index, 70);

		(function(){
			lem.element.fade(0);
			var coords = lem.element.getPosition();
			this.coords = this.element.getCoordinates();
			this.firework(coords.x + this.options.spriteWidth / 2, coords.y + this.options.spriteHeight / 2);
			lem.exploding = false;
		}).delay(1000, this);

	},

	firework: function(x, y){
		this.num_firework++;
		var i = this.fireworks.length;
		var colors = ['#ffffff', '#ffff00', '#00ff00', '#ff0000', '#0000ff', '#ff00ff', '#00ffff'], fw = [];

		var j = 20, f, a, s;

		while (j--){
			a = Math.random() * 6.294;
			s = (Math.random() > .6) ? 4 : Math.random() * 4;

			f = new Element("div", {
				"class": "spark",
				styles: {
					background: colors[Math.floor(Math.random() * 7)],
					top: y,
					left: x
				},
				y: y,
				x: x,
				dx: s * Math.sin(a),
				dy: s * Math.cos(a) - 4
			}).inject(this.element);

			fw[j] = f;

		}
		this.fireworks.push(fw);
		this.running = false;

		this.runFirework(fw);
	},

	runFirework: function(fw){
		fw.each(function(f){
			if (typeOf(f) !== "element")
				return;

			var y = f.get("y").toFloat(), x = f.get("x").toFloat(), dx = f.get("dx").toFloat(), dy = f.get("dy").toFloat();
			// console.log(y, x, dy, dx);
			y += dy += 0.18;
			x += dx;

			f.set({
				y: y,
				x: x
			});

			var op = f.getStyle("opacity").toFloat() - .05;
			if (y < this.coords.top || y > this.coords.bottom || x < this.coords.left || x > this.coords.right || op <= 0){
				// console.info("disposing of ", f, y);
				f.destroy();
				fw.erase(f);
			}
			else {

				f.set({
					styles: {
						top: y,
						left: x
					},
					opacity: op
				});

			}
		}, this);

		if (fw.length){
			(function(){
				this.runFirework(fw);
			}).delay(this.options.drawSpeed, this);
		}
	}
});


if (Browser.Engine.trident && Browser.Engine.version < 6){
	// breaks. bye.
}
else {
	window.addEvent("domready", function(){

		var letters = ["D", "R", "A", "G", "G", "E", "D"], done = 0;
		var lems = new lemmings({
			element: "inner",
			onArrive: function(index){
				this.loadSprite(index, "explode", [3, 4, 5].getRandom());
				this.say(index, letters[index]);
				this.bored(index);
				done++;
				if (done == letters.length){
					this.fireEvent("stage1", index);
				}
			},
			onStage1: function(index){
				var done = 0;
				(function(){
					this.lemmings.each(function(el, index){
						if (index >= 1){
							el.element.getFirst().fade(0);
							this.loadSprite(index, 'l', [1, 4, 7].getRandom());
							clearTimeout(el.timer);
							el.timer = (function(){
								this.say(index);
								(function(){
									this.shutUp(index);
									this.loadSprite(index, "explode", [3, 4, 5].getRandom());
									this.bored(index);
									done++;
									if (done >= this.lemmings.length - 1){
										this.fireEvent("stage2", 0);
									}
								}).delay(3000, this);

							}).delay(Number.random(1000, 3000), this);
						}
					}, this);
				}).delay(2000, this);

			},

			onStage2: function(index){
				this.explode(0);
				(function(){
					var lem = this.lemmings[0];
					this.loadSprite(index, 'r');
					lem.exploding = false;
					lem.direction = 'r';
					lem.pos = this.options.spriteWidth.neg();
					lem.element.empty().setStyles({
						"opacity": 1,
						marginLeft: lem.pos
					});
					letters[0] = 'F';
					this.goTo(0, 50);

					this.removeEvents("arrive").addEvent("arrive", function(index){
						this.bored(index);
						this.fireEvent("finish", index);
					});

				}).delay(5000, this);


			},

			onFinish: function(index){
				this.loadSprite(index, "explode", [3, 4, 5].getRandom());
				this.lemmings.each(function(el, index){
					this.say.delay(index * 300, this, [index, letters[index]]);

					this.removeEvents("arrive");
					(function(){
						this.shutUp(index);
						el.direction = ["l", "r"].getRandom();
						this.loadSprite(index, el.direction, 0);
						this.move(index);
					}).delay(Number.random(4000, 10000), this);
				}, this);
			}

		});

		letters.each(function(el, index){
			var lemId = lems.create(["r", "l"].getRandom(), Number.random(0, this.elementSize.x - this.options.spriteWidth), 1);
			lems.goTo(lemId, 50 + index * 25);
		}, lems);

	});
} // if catch block.